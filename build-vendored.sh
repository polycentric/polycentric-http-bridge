#!/bin/bash
set -xe

cd polycentric
sh version.sh
make proto
cd packages/polycentric-core
npm run build
cd ../polycentric-leveldb
npm run build

