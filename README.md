# Polycentric HTTP Bridge

The Polycentric HTTP Bridge allows programmatic control of a Polycentric client via a REST API.

## Configuration

```bash
export POLYCENTRIC_HTTP_BRIDGE_PORT=... # default 5005
export POLYCENTRIC_HTTP_BRIDGE_STATE_DIRECTORY=... # default ./state
```

## Build Quick start

```bash
git clone --recursive git@gitlab.futo.org:polycentric/polycentric.git
./build-vendored.sh
npm install
npm run build
npm start
```

## API

```bash
# Set Username
curl -H 'Content-Type: text/plain' -X POST http://localhost:5005/username --data 'Polycentric HTTP Bridge'
# Set Description
curl -H 'Content-Type: text/plain' -X POST http://localhost:5005/description --data 'I am a bridge'
# Add Server
curl -H 'Content-Type: text/plain' -X POST http://localhost:5005/server --data 'https://srv1-prod.polycentric.io'
# Remove Server
curl -H 'Content-Type: text/plain' -X DELETE http://localhost:5005/server --data 'https://srv1-prod.polycentric.io'
# Post
curl -H 'Content-Type: text/plain' -X POST http://localhost:5005/post --data 'Hello World!'
# Get a Polycentric link to this System
curl http://localhost:5005/link
# Set Avatar
curl -H 'Content-Type: application/octet-stream' -X POST http://localhost:5005/avatar --data-binary @avatar.jpg
```
