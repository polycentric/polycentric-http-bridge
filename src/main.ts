import express from 'express';
import Sharp from 'sharp';
import Long from 'long';

import * as Core from '@polycentric/polycentric-core';
import * as LevelDB from '@polycentric/polycentric-leveldb';

async function loadProcessHandle(
    directory: string,
): Promise<Core.ProcessHandle.ProcessHandle> {
    const persistenceDriver = LevelDB.createPersistenceDriverLevelDB(directory);
    const metaStore = await Core.MetaStore.createMetaStore(persistenceDriver);
    const activeStore = await metaStore.getActiveStore();

    if (activeStore) {
        console.log('Loading existing system');
        const level = await metaStore.openStore(
            activeStore.system,
            activeStore.version,
        );
        const store = new Core.Store.Store(level);
        const handle = await Core.ProcessHandle.ProcessHandle.load(store);
        return handle;
    } else {
        console.log('Generating new system');
        const handle = await Core.ProcessHandle.createProcessHandle(metaStore);
        await metaStore.setActiveStore(handle.system(), 0);
        return handle;
    }
}

async function main() {
    console.log('Polycentric HTTP Bridge');

    const port = Number(process.env.POLYCENTRIC_HTTP_BRIDGE_PORT ?? '5005');
    const stateDirectory =
        process.env.POLYCENTRIC_HTTP_BRIDGE_STATE_DIRECTORY ?? './state';

    console.log('Using port:', port);
    console.log('Using state directory:', stateDirectory);

    const handle = await loadProcessHandle(stateDirectory);

    console.log(
        `System loaded (keyType: ${handle.system().keyType}`,
        `key: ${Buffer.from(handle.system().key).toString('base64')})`,
    );

    const app = express();

    app.use(express.text({ type: 'text/plain' }));
    app.use(express.raw({ type: 'application/octet-stream', limit: '10mb' }));

    app.get('/link', async (req, res) => {
        res.send(
            'https://polycentric.io/user/' +
                (await Core.ProcessHandle.makeSystemLink(
                    handle,
                    handle.system(),
                )),
        );
    });

    app.post('/username', async (req, res) => {
        console.log('Setting username to:', req.body);
        await handle.setUsername(req.body);
        res.send('');
    });

    app.post('/description', async (req, res) => {
        console.log('Setting description to:', req.body);
        await handle.setDescription(req.body);
        res.send('');
    });

    app.post('/server', async (req, res) => {
        console.log('Adding server:', req.body);
        await handle.addServer(req.body);
        res.send('');
    });

    app.delete('/server', async (req, res) => {
        console.log('Removing server:', req.body);
        await handle.removeServer(req.body);
        res.send('');
    });

    app.post('/post', async (req, res) => {
        console.log('Posting message:', req.body);
        await handle.post(req.body);
        res.send('');
    });

    app.post('/avatar', async (req, res) => {
        console.log('Setting avatar');

        const requestImage = await req.body;

        const resolutions: number[] = [256, 128, 32];

        const imageBundle: Core.Protocol.ImageBundle = {
            imageManifests: [],
        };

        for (const resolution of resolutions) {
            const image = await Sharp(requestImage)
                .resize(resolution)
                .jpeg()
                .toBuffer();

            const imageRanges = await handle.publishBlob(image);

            imageBundle.imageManifests.push({
                mime: 'image/jpeg',
                width: Long.fromNumber(resolution),
                height: Long.fromNumber(resolution),
                byteCount: Long.fromNumber(image.length),
                process: handle.process(),
                sections: imageRanges,
            });
        }

        await handle.setAvatar(imageBundle);

        res.send('');
    });

    app.listen(port);
}

main();
